import {useState} from 'react'
import '../assets/css/main.css'
import '../assets/css/button.css'
const RoundedCheckboxGroup = ({options, radio, aromatic, setAromatic}) => {

    const [selectedValues, setSelectedValues] = useState([])
    console.log(selectedValues)
    return (
        <div className="rates">
            {options.map(({label, value}) => {
                const index = selectedValues.indexOf(value);
                const isSelected = index !== -1;

                return (
                    <div key={value} style={{marginBottom: 15}}>
                        <div  style={{
                        width: '37px',
                        height: '37px',
                        borderRadius: '50%',
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        borderStyle: 'solid',
                        borderWidth: isSelected ? 1 : 0,
                        backgroundColor: 'none',
                        borderColor: '#0362D6',

                    }} >
                        <button
                            className="button-rates"
                            style={isSelected? {backgroundColor: '#0362D6'} : {backgroundColor: '#DEDCDF'}}
                            
                            onClick={() => {
                                if(radio) {
                                    setSelectedValues([value]);
                                } else {
                                    if (isSelected) {
                                        selectedValues.splice(index,1)
                                    } else {
                                        selectedValues.push(value)
                                    }
                                    setAromatic([...selectedValues]);
                                }
                                
                            }}
                        >{label}</button>
                    </div>
                    </div>
                )
            })}
        </div>
    )
}

export default RoundedCheckboxGroup;