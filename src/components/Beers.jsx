import React, {useEffect, useState} from 'react'
import BeerPreview from './BeerPreview.jsx'
import {searchBeer, getAllBeers} from '.././services/main.js'
import { useHistory } from 'react-router'
import { ArrowLeft } from 'react-feather'
import ContentLoader from "react-content-loader"
import Error from './Error.jsx'
import '../assets/css/main.css'
import ErrorBoundary from './ErrorBoundary.js'
const Beers = ({location, props}) => {
    
    const [data, setData] = useState([])
    const [loading, setLoading] = useState(true)
    const history = useHistory();
    const goBack = () => history.goBack();

    useEffect( () => {
            const query1 = async () => {
                    const result = await getAllBeers()
                        setLoading(false)
                        setData(result)
            }

            const query2 = async () => {
                const result = await searchBeer({...location.state})
                setLoading(false)
                setData(result)
            }

            if(!location.state) {
                query1()
            }else {
                query2()
            }
            
        }, [])
       
    
        if(!data.length) {
            return <Error />
        } else {

            return(
                <ErrorBoundary>
                    <div className="bg">
                        <ArrowLeft onClick={goBack} size={50} style={{cursor: 'pointer', color: 'white', margin: 5}} />
                        
                        {loading ? (<div style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                        <ContentLoader
                            viewBox="0 0 400 160"
                            height={500}
                            width={500}
                            backgroundColor="transparent"
                            {...props}
                        >
                            <circle cx="150" cy="86" r="8" />
                            <circle cx="194" cy="86" r="8" />
                            <circle cx="238" cy="86" r="8" />
                        </ContentLoader>
                        </div>) : (
                            <div>
                                <span style={{textAlign: 'center', color: 'white'}}><h1>Les bières</h1></span>
                            <div className="beers-wrap" >
                                {data && data.map((beer) => (
                                <BeerPreview  key={beer._id} {...beer} />
                                ))}
                                    
                            </div>
                            </div>
                        )}
                    </div>
                </ErrorBoundary>
            )
        }
        
        
        
    
}

export default Beers