import React from 'react'
import '../../assets/css/fonts.css'
import '../../assets/css/button.css'
import './beertastor.css'
import FlavorCheckboxGroup from '../FlavorCheckboxGroup.jsx'
import { useAromatic } from '../../hooks/useAromatic.js'
import { useBitter } from '../../hooks/useBitter.js'
import AromaticRadioGroup from '../AromaticRadioGroup.jsx'
import BitterRadioGroup from '../BitterRadioGroup.jsx'
import { useFlavor } from '../../hooks/useFlavor'
import { Link } from 'react-router-dom'
import ErrorBoundary from '../ErrorBoundary'
const BeerTastor = () => {

    const aromatic = useAromatic();
    const bitter = useBitter();
    const flavor = useFlavor();
   
    const aromaticRate = [{value: 1, label: '1'}, {value: 2, label: '2'}, {value: 3, label: '3'}, {value: 4, label: '4'}, {value: 5, label: '5'}]
    const bitterRate = [{value: 1, label: '1'}, {value: 2, label: '2'}, {value: 3, label: '3'}, {value: 4, label: '4'}, {value: 5, label: '5'}]
    const aromas = [{value: 'tropical_fruits', label: 'Fruits Tropicaux'}, {value: 'red_fruits', label: 'Fruits Rouges'}, {value: 'lemon', label: 'Citron'}, {value: 'chocolate', label: 'Chocolat'}, {value: 'coffee', label: 'Café'}, {value: 'honey', label: 'Miel'}]
    
    const query = {aromatic: aromatic.aromatic, bitter: bitter.bitter, flavor: flavor.flavor}

    return (
        <ErrorBoundary>
            <div style={{width: 'auto', height:'auto', borderRadius: 25, backgroundColor: 'white', padding: 20, margin: 10, boxShadow: "1px 1px 23px 5px rgba(0,0,0,0.47)"}}>
                <div className="palate">
                    <div style={{textAlign: 'center'}}>
                    <h4>Intensité Aromatique</h4>
                    <AromaticRadioGroup options={aromaticRate} radio {...aromatic}  />
                    </div>
                    <div className="bitterness" style={{textAlign: 'center'}}>
                    <h4>Intensité Amertume</h4>
                    <BitterRadioGroup options ={bitterRate} radio {...bitter}  />
                    </div>
                
                </div>
                    <div className="flavor">
                        <h4>Arômes</h4>
                        <span style={{display:'flex', flexDirection: 'row'}}>
                        <FlavorCheckboxGroup options={aromas} {...flavor} />
                        </span>
                    </div>
                    
                    <Link to={{
                        pathname: "/beers",
                        state: query
                    }} className="button-send">GO !</Link>
                    {/* <Link className="button-send" style={{backgroundColor: '#DEDCDF', color: 'black'}} to='/beers'>Toutes les bières</Link> */}
            </div>
        </ErrorBoundary>
    )
}

export default BeerTastor;