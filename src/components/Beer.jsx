import {useEffect, useState} from 'react'
import { useHistory, useParams } from "react-router";
import { getBeer } from "../services/main";
import CardBeer from './CardBeer';
import { ArrowLeft } from 'react-feather'
import ContentLoader from "react-content-loader"
import ErrorBoundary from './ErrorBoundary';

const Beer = ({props}) => {
  
  const { id }= useParams()
  const [data, setData] = useState([]);
 const history = useHistory();
 const [loading, setLoading] = useState(true)

  useEffect(() => {
    const displayBeer = async () => {
      const result = await getBeer(id)
      setData(result)
      setLoading(false)
    }
    displayBeer();
  }, [])

  const goBack = () => history.goBack();

  return (
    <ErrorBoundary>
      <div className="bg">
        <div>
          <ArrowLeft onClick={goBack} size={50} style={{cursor: 'pointer', color: 'white', margin: 5}} />
          {loading ? (<div style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                <ContentLoader
                    viewBox="0 0 400 160"
                    height={500}
                    width={500}
                    backgroundColor="transparent"
                    {...props}
                >
                    <circle cx="150" cy="86" r="8" />
                    <circle cx="194" cy="86" r="8" />
                    <circle cx="238" cy="86" r="8" />
                </ContentLoader>
                </div>) : (
                  <span style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                  <CardBeer data={[...data]} />
                  </span>
                )}
          </div>
        </div>
    </ErrorBoundary>
  );
};

export default Beer