import '../assets/css/main.css'
import '../assets/css/button.css'
const BitterlessRadioGroup = ({options, radio, bitter, setBitter, }) => {

    return (
        <div className="rates" style={{flexDirection: 'row'}}>
            {options.map(({label, value}) => {
                const index = bitter.indexOf(value);
                const isSelected = index !== -1;

                return (
                    <div key={value} style={{marginRight: 15}}>
                        <div  style={{
                        width: '37px',
                        height: '37px',
                        borderRadius: '50%',
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        borderStyle: 'solid',
                        borderWidth: isSelected ? 1 : 0,
                        backgroundColor: 'none',
                        borderColor: '#0362D6',
                    }} >
                        <button
                            className="button-rates"
                            style={isSelected? {backgroundColor: '#000'} : {backgroundColor: '#DEDCDF'}}
                            onClick={() => {
                                if(radio) {
                                    setBitter([value]);
                                } else {
                                    if (isSelected) {
                                        bitter.splice(index,1)
                                    } else {
                                        bitter.push(value)
                                    }
                                    setBitter([...bitter]);
                                }
                                
                            }}
                        >{label}</button>
                    </div>
                    </div>
                )
            })}
        </div>
    )
}

export default BitterlessRadioGroup;