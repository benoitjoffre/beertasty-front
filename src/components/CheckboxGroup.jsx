import {useState} from 'react'
import '../assets/css/main.css'
import '../assets/css/button.css'
const CheckboxGroup = ({options, radio}) => {

    const [selectedValues, setSelectedValues] = useState([])
    
    return (
        <div className="multi">
            {options.map(({label, value}) => {
                const index = selectedValues.indexOf(value);
                const isSelected = index !== -1;

                return (
                    <button
                        className="button-multi"
                        style={isSelected? {backgroundColor: '#0362D6'} : {backgroundColor: '#DEDCDF'}}
                        key={value}
                        onClick={() => {
                            if(radio) {
                                setSelectedValues([value]);
                            } else {
                                if (isSelected) {
                                    selectedValues.splice(index,1)
                                } else {
                                    selectedValues.push(value)
                                }
                                setSelectedValues([...selectedValues]);
                            }
                            
                        }}
                    >{label}</button>
                )
            })}
        </div>
    )
}

export default CheckboxGroup;