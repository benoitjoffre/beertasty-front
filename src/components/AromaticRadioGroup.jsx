import '../assets/css/main.css'
import '../assets/css/button.css'
const AromaticRadioGroup = ({options, radio, aromatic, setAromatic}) => {
    
    return (
        <div className="rates" style={{flexDirection: 'row'}}>
            {options.map(({label, value}) => {
                const index = aromatic.indexOf(value);
                
                const isSelected = index !== -1;

                return (
                    <div key={value} style={{marginRight: 15}}>
                        <div  style={{
                        width: '37px',
                        height: '37px',
                        borderRadius: '50%',
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        borderStyle: 'solid',
                        borderWidth: isSelected ? 1 : 0,
                        backgroundColor: 'none',
                        borderColor: '#0362D6',

                    }} >
                        <button
                            className="button-rates"
                            style={isSelected? {backgroundColor: '#000'} : {backgroundColor: '#DEDCDF'}}
                            
                            onClick={() => {
                                if(radio) {
                                    setAromatic([value]);
                                } else {
                                    if (isSelected) {
                                        aromatic.splice(index,1)
                                    } else {
                                        aromatic.push(value)
                                    }
                                    setAromatic([...aromatic]);
                                }
                                
                            }}
                        >{label}</button>
                    </div>
                    </div>
                )
            })}
        </div>
    )
}

export default AromaticRadioGroup;