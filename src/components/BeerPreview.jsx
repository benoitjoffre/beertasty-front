import '../assets/css/main.css'
import {Link} from 'react-router-dom'
import ErrorBoundary from './ErrorBoundary';
const beerPreview = ({ beername, description, breweryname, alcohol, _id }) => (
	<ErrorBoundary>
		<div className="card">
			<span className="beer-preview-info">
				<h3>{beername}</h3>
				<h5 style={{margin:0}}>{breweryname}</h5>
				<p>{description.substr(0, 100)}...</p>
				<p style={{fontWeight: 'bold'}}>{alcohol} % vol alcool</p>
			</span>

		<span className="button-beer-preview-wrap"><Link className="button-beer-preview" to={'/beers/' + `${_id}`}>VOIR</Link></span>
		</div>
	</ErrorBoundary>
);

export default beerPreview