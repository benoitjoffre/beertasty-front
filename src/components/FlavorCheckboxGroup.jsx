import '../assets/css/main.css'
import '../assets/css/button.css'
const FlavorCheckboxGroup = ({options, flavor, setFlavor}) => {
    
    return (
        <div className="multi">
            {options.map(({label, value}) => {
                const index = flavor.indexOf(value);
                const isSelected = index !== -1;

                return (
                    <button
                        className="button-multi"
                        style={isSelected? {backgroundColor: '#000'} : {backgroundColor: '#DEDCDF'}}
                        key={value}
                        onClick={() => {
                            
                                if (isSelected) {
                                    flavor.splice(index,1)
                                } else {
                                    flavor.push(value)
                                }
                                setFlavor([...flavor]);
                            
                        }}
                    >{label}</button>
                )
            })}
        </div>
    )
}

export default FlavorCheckboxGroup;