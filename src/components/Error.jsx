import { useEffect, useState } from "react"
import { getAllBeers } from "../services/main"
import BeerPreview from "./BeerPreview"
import { useHistory } from 'react-router'
import { ArrowLeft } from 'react-feather'
import '../assets/css/main.css'
import ContentLoader from "react-content-loader"

const Error = ({props}) => {

    const history = useHistory();
    const goBack = () => history.goBack();
    const [data, setData] = useState()
    const [loading, setLoading] = useState(true)

    useEffect(() => {
       const query = async () => {
        const result = await getAllBeers()
        setData(result)
        setLoading(false)
       }
       query()
    }, [])
    
 

    return(
        <div className="bg">
            <ArrowLeft onClick={goBack} size={50} style={{cursor: 'pointer', color: 'white', margin: 5}} />
            <span style={{textAlign: 'center', color: 'white'}}>

                {loading ? (
                    
                        <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                        <ContentLoader
                            viewBox="0 0 400 160"
                            height={500}
                            width={500}
                            backgroundColor="transparent"
                            {...props}
                        >
                            <circle cx="150" cy="86" r="8" />
                            <circle cx="194" cy="86" r="8" />
                            <circle cx="238" cy="86" r="8" />
                        </ContentLoader>
                        </div>
                ) : (
                <div>
                    <h1>Désolé, Il n'existe pas encore de bières avec tes choix !</h1>
                    <h3>Nous pouvons te proposer ces bières en attendant !</h3>
                    <div style={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center', alignItems: 'center'}}>
                        {data.slice(0, 5).map((beer) => (
                            <BeerPreview  key={beer._id} {...beer} />
                        ))}
                    </div>
                </div>
                )}
            </span>
        </div>
    )
}

export default Error