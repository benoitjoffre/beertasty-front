import '../assets/css/main.css'
import '../assets/css/button.css'
const CardBeer = ({data}) => {
    
    return (
        <div className="card-beer">
            <h1 style={{margin: 0}}>{data[0] && data[0].beername}</h1>
            <h2 style={{marginBottom: 30}}>Brasserie : {data[0] && data[0].breweryname}</h2>
            <p>{data[0] && data[0].description}</p>
            <p style={{fontWeight: 'bold'}}>{data[0] && data[0].alcohol} % vol alcool</p>

            <span style={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap' }}>
            <p style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>Amertume  <span className="button-rates-beer">{data[0] && data[0].bitter}</span></p>
            <p style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>Aromatique  <span className="button-rates-beer">{data[0] && data[0].aromatic}</span></p>
            </span>
        </div>
    )
}

export default CardBeer