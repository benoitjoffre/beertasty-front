import React from 'react'
import '../assets/css/main.css'
import '../assets/css/fonts.css'
import '../assets/css/button.css'
import { Link } from 'react-router-dom'

const Onboarding = () => {
    return (
        <div className="bg">
            <div className="onboarding">
                <span className="text-onboarding">
                <h1 className="title" style={{color: 'white'}}>BEER</h1>
                <h1 className="title">TASTY.</h1>
                </span>
                <span className="button-onboarding-wrap"><Link className="button-onboarding" to="/home">ENTRER</Link></span>
            </div>
        </div>
    )
}

export default Onboarding;