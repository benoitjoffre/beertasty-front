import '../assets/css/fonts.css'
import '../assets/css/main.css'
import BeerTastor from '../components/beertastor/BeerTastor';



const Home = () => {

  return (
    <div className="bg">
      <div className="beertastor" >
        <div className="subtitle">
          <h2 style={{color: 'white'}}>BEERTASTOR.</h2>
        </div>
        <div>
          <BeerTastor />
        </div>
      </div>
    </div>
  );
}

export default Home;