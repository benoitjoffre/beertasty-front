export const searchBeer =  async (data) => {
    const response = await fetch(`https://salty-lake-45232.herokuapp.com/api/beers?aromatic=${data.aromatic}&bitter=${data.bitter}&flavor=${data.flavor}`, {
         method: 'GET',
         headers: { 'Content-Type': 'application/json'},
     })
     return await handleResponse(response)
 }

 export const getAllBeers = async () => {
    const response = await fetch(`https://salty-lake-45232.herokuapp.com/api/beers`, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json'},
    })
    return await handleResponse(response)
}

export const getBeer = async (id) => {
    const response = await fetch(`https://salty-lake-45232.herokuapp.com/api/beers/${id}`, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json'},
    })
    return await handleResponse(response)
}
 const handleResponse = async response => {
    if (response.status >= 400 && response.status <= 499) {
        throw new Error("Le choix n'est pas valide ou il n'existe pas de bières !")
    }
    if (response.status >= 500 && response.status <= 599) {
        throw new Error("Erreur 5XX")
    }
    if (response.status >= 300 && response.status <= 399) {
        throw new Error("Erreur 3XX")
    }
    return await response.json()
}