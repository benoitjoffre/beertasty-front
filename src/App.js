import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Beers from "./components/Beers";
import Beer from "./components/Beer";
import Home from "./components/Home";
import Onboarding from './components/Onboarding'
const App = () => {
  return(
    <Router>
        <Switch>
          <Route  default exact path="/" component={Onboarding}/>
            <Route exact path="/home" component={Home} />
            <Route exact path="/beers" component={Beers} />
            <Route exact path="/beers/:id" component={Beer} />
        </Switch>
    </Router>
  )
}

export default App;