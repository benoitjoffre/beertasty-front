import {useState} from 'react'

export const useAromatic = () => {
    const [aromatic, setAromatic] = useState([])
    return {
        aromatic,
        setAromatic
    }
}