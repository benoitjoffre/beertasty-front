import {useState} from 'react'

export const useBitter = () => {
    const [bitter, setBitter] = useState([])
    return {
        bitter,
        setBitter
    }
}