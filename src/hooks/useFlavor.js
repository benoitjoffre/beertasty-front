import {useState} from 'react'

export const useFlavor = () => {
    const [flavor, setFlavor] = useState([])
    return {
        flavor,
        setFlavor
    }
}